
const express = require('express');
const bodyParser = require('body-parser')
const cors = require ('cors')
var fs = require("fs")
const app = express();
const PORT = process.env.PORT || 8888;
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }));
app.use(cors());
app.use(express.json());
app.listen(PORT, () => console.log(`Listening on ${ PORT }`));
var amqp = require('amqplib/callback_api');
var username = "";
var pass = "";
app.post('/login', (req, res) => {
	if(req.body.username==undefined||req.body.username==null)
	{
		res.json({code:401, status: "unauthorize Token"});
		return;
	}
	username = req.body.username;
	pass = req.body.password;
var json = {code:200, status: "success"}
res.json(json);
});
app.post('/createHub', (req, res) => {
	if(req.body.key==undefined||req.body.key==null||req.body.key!="129bd8d2d9ac4c9ba4a74d4921c3817f")
	{
		res.json({code:401, status: "unauthorize Token"});
		return;
	}
	if(username=="")
	{
		res.json({code:402, status: "Not login"});
	}
try
{
	var url = 'amqp://'+username+':'+pass+'@13.229.136.251:5672';//console.log(url);
		amqp.connect(url, function(error0, connection) {
	  if (error0) {
		json.status = "Unknown Error"
	  }
	  connection.createChannel(function(error1, channel) {
		if (error1) {
		  json.status = "Login Failed"
		}
		var queue = 'dosqueue';
		var msg = JSON.stringify(req.body);

		channel.assertQueue(queue, {
		  durable: true
		});

		channel.sendToQueue(queue, Buffer.from(msg));
		
		console.log(" [x] Sent %s", msg);
	  });
	});
}
catch(ex)
{
	res.send({code:301, status: "error"});
}
var json = {code:200, status: "success"}
res.json(json);
});

